<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_blogus_db' );

/** Database username */
define( 'DB_USER', 'wp_blogus_user' );

/** Database password */
define( 'DB_PASSWORD', 'wp_blogus_pw' );

/** Database hostname */
define( 'DB_HOST', 'localhost:3306' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '1kk4u6xJ,g qMOd=?N@}=IF{@<+aK_NBMfjorD(]5YgHBP{m[jsx|K5N!h5 `}uM');
define('SECURE_AUTH_KEY', 'lCd?iGt5&pKd=W7Eh%1`6LTfEhgtUlm-dQ4_[e§Z_{{[DO1F+)o%a-aC{nD§eh8Q');
define('LOGGED_IN_KEY', 'W2{-b|&O=`q`[~Dj@(<lu(`S{x&r[0T4wv%[RAZVisy,p}0-Qpkoz/t}dP|1_302');
define('NONCE_KEY', '.Kw3tT;~CIh|T;@Ep4)uAjqOa4_zAt7MqFCePNGxsYX9AJp/1/pjR?@(?sK$nD>@');
define('AUTH_SALT', 'g5L`R09yQlf&OXM&28r4XN-ws6HRr,p6,dGG^Mv<7=dth]z-9RbLO7k~=gHeu_$r');
define('SECURE_AUTH_SALT', '=5~@b/SE,!RCME28-3?lFSV{NCCd?7e=SX@EO@/KCe@nX$dhm,VydU6y| !Uhvj(');
define('LOGGED_IN_SALT', 'bZ@o+Q§qnVICp6o)erdcTS/§N_Pz2n:6vZz<ccC@eCm4d@TXg_RP2]t<§fG_|fiw');
define('NONCE_SALT', 'rd%n48rdyK^|9fXZ Z;$o(KvqAeBm§{2,g>BsFGdH1{iqN@q kJ§Z)O@`!+00O^@');

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
