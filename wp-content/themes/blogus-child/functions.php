<?php
/**
 * Include All Inc Files
 *
 */
require_once __DIR__ . '/inc/books/index.php';
require_once __DIR__ . '/inc/widgets/index.php';
require_once __DIR__ . '/inc/gutenberg/index.php';
require_once __DIR__ . '/inc/post-type/index.php';

/**
 * Include Parent Theme Style and Script
 *
 */

add_action('wp_enqueue_scripts', 'enqueue_script');
function enqueue_script()
{
    wp_enqueue_style('parent-theme-style', get_template_directory_uri() . '/style.css');
    wp_enqueue_script('parent-theme-script', get_template_directory_uri() . '/js/custom.js', array(), '1.0', true);
    wp_localize_script('parent-theme-script', 'ajax_object',
        array('ajax_url' => admin_url('admin-ajax.php'))
    );
}


/**
 * Register Gutenberg Blocks Options / scripts
 *
 */

add_action('init', 'register_options_blocks');
if (!function_exists('register_options_blocks')) {
    function register_options_blocks()
    {
        foreach (get_glob_folders_path('/template-parts/gutenberg/*/register.php') as $file_path) {
            require_once(get_theme_file_path($file_path));
        }
    }
}


/**
 * Register ACF Gutenberg Blocks
 *
 */

add_action('acf/init', 'register_acf_blocks');
if (!function_exists('register_acf_blocks')) {
    function register_acf_blocks()
    {
        foreach (get_glob_folders_path('/template-parts/gutenberg/acf-*/block.json') as $file_path) {
            register_block_type(get_theme_file_path($file_path));
        }
    }

}




