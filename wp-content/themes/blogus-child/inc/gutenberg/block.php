<?php
/**
 * Global Block Class
 */
class Block {
	private $_block = null;

	public function __construct( $block ) {
		$this->_block = $block;
	}

	public function id( $custom = '' ) {
		$id = '';

		if ( ! empty( $this->_block['anchor'] ) || ! empty( $custom ) ) {
			$id_arr = [];

			if ( ! empty( $this->_block['anchor'] )) {
				$id_arr[] = $this->_block['anchor'];
			}

			if ( ! empty( $custom ) ) {
				$id_arr[] = "$custom";
			}

			if ( ! empty( $id_arr ) ) {
				$id = ' id="' . implode( ' ', $id_arr ) . '"';
			}
		}

		return $id;
	}

	public function class( $custom = '' ) {
		$name =  str_replace( 'acf/', '', $this->_block['name'] );
		$class_arr = [$name];

		if ( ! empty( $custom ) ) {
			$class_arr[] = $custom;
		}

		if ( isset( $this->_block['className'] ) ) {
			$class_arr[] = $this->_block['className'];
		}


		$class = 'class="' . implode( ' ', $class_arr ) . '"';

		return $class;
	}
	public function name() {
		return str_replace( 'acf/', '', $this->_block['name'] );
	}

	public function attr( $custom_class = '', $custom_id = '', $custom_attr = '' ) {
		$attributes = $this->class($custom_class) . $this->id($custom_id);

		if ( ! empty($custom_attr) ) {
			$attributes .= " $custom_attr";
		}

		return $attributes;
	}
}
