<?php

require_once 'block.php';
require_once 'category.php';

/**
 * Get Glob Folder Template Paths
 */
if ( ! function_exists( 'get_glob_folders_path' ) ) {

    function get_glob_folders_path( $path = '' ) {
        $theme_info  = wp_get_theme();
        $theme_part  = ( $theme_info->stylesheet == $theme_info->template ) ? $theme_info->template : $theme_info->stylesheet . ',' . $theme_info->template;
        $file_paths  = glob( get_theme_root() . '/{' . $theme_part . '}' . $path, GLOB_BRACE );

        if ( ! empty( $file_paths ) ) {
            foreach ( $file_paths as &$file_path ) {
                $file_path = str_replace( [
                    get_stylesheet_directory(),
                    get_template_directory()
                ], '', $file_path );
            }
        }

        return $file_paths;
    }

}


