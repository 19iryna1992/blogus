<?php

/**
 * Books_Filter Class
 *
 */

class Books_Filter extends WP_Widget
{
    public function __construct()
    {
        parent::__construct(
            'books_filter_widget',
            'Books Filter',
            array(
                'description' => 'This is my custom widget.',
            )
        );
    }

    public function widget($args, $instance)
    {
        $book_genres = get_terms(array(
            'taxonomy' => 'book_category',
        ));
        echo $args['before_widget'];
        if (!empty($book_genres)) {
            ?>

            <div class="widget-content">
                <ul>
                    <li>
                        <a href="#" class="js-book-filter-item" data-term-id="<?php echo '0' ?>">
                            <?php _e('All Category', 'blogus') ?>
                        </a>
                    </li>
                    <?php foreach ($book_genres as $book_genre): ?>
                        <li>
                            <a href="#" class="js-book-filter-item" data-term-id="<?php echo $book_genre->term_id ?>">
                                <?php echo $book_genre->name ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <?php
        }
        echo $args['after_widget'];

    }
}

// Register custom widget
function register_books_filter_widget()
{
    register_widget('Books_Filter');
}

add_action('widgets_init', 'register_books_filter_widget');
?>