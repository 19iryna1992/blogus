<?php
/* Ajax Book Filter Files */

add_action('wp_ajax_book_filter', 'book_filter');
add_action('wp_ajax_nopriv_book_filter', 'book_filter');

function book_filter()
{
    $term_id = $_POST['termId'];

    $book_args = array(
        'post_type' => 'book',
        'posts_per_page' => -1,
    );

    if ($term_id != 0) {
        $book_args['tax_query'] = array(
            array(
                'taxonomy' => 'book_category',
                'field' => 'id',
                'terms' => (int)$term_id,
            ),
        );
    }

    $book_query = new WP_Query($book_args);

    if ($book_query->have_posts()) {
        while ($book_query->have_posts()) {
            $book_query->the_post();
            ?>
            <div class="bs-card-box padding-20 mb-5">
                <?php get_template_part('template-parts/components/books/book-preview'); ?>
            </div>
            <?php
        }
        wp_reset_postdata();
    } else {
        echo 'No posts found';
    }

    wp_die();
}