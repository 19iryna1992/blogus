<?php
/**
 * Book Class
 *
 */
class Book
{
    private $isbn;
    private $data;

    public $title = '';
    public $thumbnails = [];
    public $authors = [];
    public $publisher = '';
    public $year = '';
    public $categories = [];


    function __construct($code_isbn)
    {
        $this->isbn = $code_isbn;
        $this->response();
    }

    /* Get Books Data */
    private function response()
    {
        $request_url = 'https://www.googleapis.com/books/v1/volumes?q=isbn:' . $this->isbn;
        $response = wp_remote_get($request_url);

        if (!is_wp_error($response)) {
            $body = wp_remote_retrieve_body($response);
            $this->data = json_decode($body);
        }

        set_transient('cached_data', $this->data, DAY_IN_SECONDS);
        $this->get_data();

    }


    /* Get Books Data Items */
    public function get_data()
    {
        if (!empty($this->data)) {
            $items = $this->data->items;
            foreach ($items as $item) {
                $this->title = $item->volumeInfo->title;
                $this->authors = $item->volumeInfo->authors;
                $this->publisher = $item->volumeInfo->publisher;
                $this->year = $item->volumeInfo->publishedDate;
                $this->categories = $item->volumeInfo->categories;
                $this->thumbnails = $item->volumeInfo->imageLinks;
            }
        }
    }
}