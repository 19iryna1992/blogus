<?php

add_action('after_switch_theme', 'sb_flush_rewrite_rules');
function sb_flush_rewrite_rules()
{
    flush_rewrite_rules();
}

/* Set Post Type Labels */
function sb_post_type_labels($singular = 'Post', $plural = 'Posts')
{
    $p_lower = strtolower($plural);
    $s_lower = strtolower($singular);

    return [
        'name'                   => $plural,
        'singular_name'          => $singular,
        'add_new_item'           => "New $singular",
        'edit_item'              => "Edit $singular",
        'view_item'              => "View $singular",
        'view_items'             => "View $plural",
        'search_items'           => "Search $plural",
        'not_found'              => "No $p_lower found",
        'not_found_in_trash'     => "No $p_lower found in trash",
        'parent_item_colon'      => "Parent $singular",
        'all_items'              => "All $plural",
        'archives'               => "$singular Archives",
        'attributes'             => "$singular Attributes",
        'insert_into_item'       => "Insert into $s_lower",
        'uploaded_to_this_item'  => "Uploaded to this $s_lower",
    ];
}

/* Register Post Type */

add_action('init', function () {

    $labels = sb_post_type_labels('Book', 'Books');

    $args = array(
        'description' => __('Book', 'sb'),
        'labels' => $labels,
        'supports' => ['title', 'editor'],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_icon'     => 'dashicons-book',
        'menu_position' => 3,
        'rewrite' => ['slug' => 'book', 'with_front' => false],
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type'    => 'post',
    );

    register_post_type('book', $args);
});


/* Register Taxonomy */

add_action('init', function () {

    $tax =  'book_category';
    $type = ['book'];

    $labels = sb_post_type_labels('Book Category', 'Book Categories');

    $args = [
        'description'         => 'Book Category',
        'labels'              => $labels,
        'hierarchical'        => true,
        'show_ui'             => true,
        'show_admin_column'   => true,
        'show_in_quick_edit'  => true,
        'show_in_menu'        => true,
        'rewrite'             => ['slug' => 'boock_category', 'with_front' => false],
    ];

    register_taxonomy($tax, $type, $args);
});
