<?php
/**
 * Single Blog Post template.
 *
 */

get_header(); ?>
    <main>
        <div class="container">
            <div class="row">
                <div class="col-12 mb-5">
                    <div class="bs-blog-post list-blog">
                        <div class="small text-xs text-start">
                            <?php get_template_part('template-parts/components/books/book-preview'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mb-5">
                    <?php get_template_part('template-parts/components/books/book-reviews'); ?>
                </div>
            </div>
        </div>
    </main>
<?php
get_footer();
