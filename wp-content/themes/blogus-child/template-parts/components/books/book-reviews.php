<?php
if (have_rows('reviews_book_post')):
    while (have_rows('reviews_book_post')): the_row();
        $name = get_sub_field('name') ? get_sub_field('name') : '';
        $data = get_sub_field('date') ? get_sub_field('date') : '';
        $review = get_sub_field('review') ? get_sub_field('review') : '';
        ?>
        <?php if ($review): ?>
            <div class="bs-card-box padding-20">
                <div class="small text-xs">
                    <?php if ($name): ?>
                        <div class="bs-author text-start">
                            <?php echo $name ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($data): ?>
                        <div class="text-start">
                            <time>
                                <?php echo $data ?>
                            </time>
                        </div>
                    <?php endif; ?>
                    <p class="text-start"><?php echo $review ?></p>
                </div>
            </div>
        <?php endif; ?>
    <?php
    endwhile;
endif;
?>