<?php
$book_id = get_the_ID();
$isbn = get_field('isbn_book_post', $book_id);
$book_data = new Book($isbn);
$link = get_permalink();
$title = $book_data->title ? $book_data->title : get_title();
$authors = $book_data->authors ? $book_data->authors : [];
$publisher = $book_data->publisher ? $book_data->publisher : '';
$year = $book_data->year ? $book_data->year : '';
$categories = $book_data->categories ? $book_data->categories : [];
$thumbnails = $book_data->thumbnails ? $book_data->thumbnails : [];
?>

<article class="p-2">
    <div class="mb-3">
        <?php if (is_single()): ?>
            <h1 class="text-center mb-5"><?php echo $title ?></h1>
        <?php else: ?>
            <h3 class="title">
                <a href="<?php echo $link ?>">
                    <?php echo $title ?>
                </a>
            </h3>
        <?php endif; ?>
    </div>
    <div class="d-flex">
        <div class="me-4">
            <?php if (!empty($thumbnails) && $thumbnails->thumbnail): ?>
                <img src="<?php echo $thumbnails->thumbnail ?>" alt="<?php echo $title ?>">
            <?php endif; ?>
        </div>
        <div class="book-preview__info">
            <?php if (!empty($authors)): ?>
                <div>
                    <small><?php _e("Author", 'blogus') ?></small>
                    <?php foreach ($authors as $author): ?>
                        <span class="text-dark"> <?php echo $author ?></span>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
            <?php if (!empty($publisher)): ?>
                <div>
                    <small><?php _e("Publisher:", 'blogus') ?></small>
                    <span class="text-dark"><?php echo $publisher ?></span>
                </div>
            <?php endif; ?>
            <?php if (!empty($year)): ?>
                <div>
                    <small><?php _e("Data:", 'blogus') ?></small>
                    <time class="text-dark"><?php echo $year ?></time>
                </div>
            <?php endif; ?>
            <?php if (!empty($categories)): ?>
                <div>
                    <?php foreach ($categories as $cat): ?>
                        <small><?php _e("Categories:", 'blogus') ?></small>
                        <span class="text-dark"><?php echo $cat ?></span>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</article>
