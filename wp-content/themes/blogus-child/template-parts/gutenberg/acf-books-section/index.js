document.addEventListener('DOMContentLoaded', function () {
    const terms = document.querySelectorAll('.js-book-filter-item');
    const bookSection = document.getElementById('js-books-section');

    terms.forEach(item => {
        item.addEventListener('click', (e) => {
            e.preventDefault();

            const termId = item.getAttribute('data-term-id');

            fetch(ajax_object.ajax_url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                body: new URLSearchParams({
                    action: 'book_filter',
                    termId,
                }),
            })
                .then(response => response.text())
                .then(data => {
                    bookSection.innerHTML = data;
                })
                .catch(error => {
                    console.error('Error fetching data:', error);
                });
        })
    });
});