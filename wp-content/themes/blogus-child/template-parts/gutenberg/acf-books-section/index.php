<?php
/**
 * Block with Books Section
 *
 * @package WordPress
 * @subpackage Blogus
 * @since blogus 1.0
 */

$block_object = new Block($block);
$attr = $block_object->attr();
$name = $block_object->name();
$id = $block['id'];

$book_args = array(
    'post_type' => 'book',
    'posts_per_page' => -1
);
$book_query = new WP_Query($book_args);
?>

<section <?php echo $attr; ?>>
    <?php
    if ($book_query->have_posts()):
        while ($book_query->have_posts()): $book_query->the_post();
            ?>
            <div class="bs-card-box padding-20 mb-5">
                <?php get_template_part('template-parts/components/books/book-preview'); ?>
            </div>
        <?php
        endwhile;
        wp_reset_postdata();
    else :
        echo 'No posts found';
    endif;
    ?>
</section>

