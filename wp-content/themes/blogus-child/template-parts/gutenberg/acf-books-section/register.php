<?php
$slug = 'acf-books-section';
$dir_path = "/template-parts/gutenberg/$slug";

wp_register_script( $slug, get_theme_file_uri( "$dir_path/index.js" ), [ ], filemtime( get_theme_file_path( "$dir_path/index.js" ) ), true );